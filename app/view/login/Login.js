Ext.define('TutorialApp.view.login.Login'), {
	extend: 'Ext.window.Window',
	xtype: 'login'


	require: [
	'TutorialApp.view.login.LoginController',
	'Ext.form.Panel'
	],

	controller: 'login',
	bodyPadding: 10,
	title: 'Login Window',
	closable: false,
	autoShow: true


	items: {
		xtype: 'form',
		reference: 'form',
		items: [{
			xtype: 'textfield',
			name: 'username',
			fieldLabel: 'Username'
			allowBlank: false
		},{
			xtype: 'displayfield',
			
		}

		]
		}

	}
});